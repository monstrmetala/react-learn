import { useState } from 'react';

export const useFetching = (call) => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState('');

  const ajax = async (...args) => {
    try {
      setIsLoading(true);
      await call(...args);
    } catch (e) {
      setError(e.message);
    } finally {
      setIsLoading(false);
    }
  }

  return [ajax, isLoading, error]
}