import React, { useState, useEffect } from 'react';
import PostService from '../API/PostService';
import PostFilter from '../component/PostFilter';
import PostForm from '../component/PostForm';
import PostList from '../component/PostList';
import MyButton from '../component/UI/button/MyButton';
import Loader from '../component/UI/Loader/Loader';
import MyModal from '../component/UI/MyModal/MyModal';
import Pagination from '../component/UI/pagination/pagination';
import { getPageCount } from '../component/utils/pages';
import { useFetching } from '../hooks/useFetching';
import { usePosts } from '../hooks/usePost';



function Posts() {
  const [posts, setPosts] = useState([]);
  const [filter, setFilter] = useState({sort: '', query: ''});
  const [modal, setModal] = useState(false);
  const sortAndSearchPosts = usePosts(posts,filter.sort, filter.query);
  const [totalPages, setTotalPages] = useState(0);
  const [limit, setLimit] = useState(10);
  const [page, setPage] = useState(1);
  
  const [fetchPosts, isPostLoading, postError] = useFetching( async (limit, page) => {
    const posts = await PostService.getAll(limit, page);
    setPosts(posts.data);

    const totalPosts = posts.headers['x-total-count'];
    setTotalPages(getPageCount(totalPosts, limit));
  });


  useEffect( () => {
    fetchPosts(limit, page);
  }, []);

  const createPost = (newPost) => {
    setPosts([...posts, newPost ]);
    setModal(false);
  };

  // Get Post from child component
  const removePost = (post) => {
    setPosts(posts.filter(p => p.id !== post.id))
  }

  const changePage = (page) => {
    setPage(page);
    fetchPosts(limit, page);
  }


  return (
    <div className="App">
      <MyButton onClick={fetchPosts} >Get Posts</MyButton>
      <MyButton onClick={() => setModal(true)} >Create Post</MyButton>
      <hr/>
      <MyModal visible={modal} setVisible={setModal}>
        <PostForm create={createPost}/>
      </MyModal>

      <PostFilter filter={filter} setFilter={setFilter} />

      {postError &&
        <h2>Ошибка: <em>{postError}</em></h2>
      }

      {
        isPostLoading
          ? <Loader />
          : <PostList remove={removePost} posts={sortAndSearchPosts} title="Список постов Junior" />
      }
      <hr/>

      <Pagination  
        page={page}
        totalPages={totalPages}
        changePage={changePage}
      />
      
    </div>
  );
}

export default Posts;
