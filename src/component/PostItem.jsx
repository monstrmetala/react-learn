import { Link } from "react-router-dom";
import MyButton from "./UI/button/MyButton";

const PostItem = (props) => {

  return (
    <div className="post">
      <div className="post__content">
        <strong>{props.post.id}. {props.post.title}</strong>
        <p>{props.post.body.substr(0, 25)}</p>
      </div>
      <div className='post__btns'>
        <Link to={`/posts/${props.post.id}`} >Open</Link>
        <MyButton onClick={()=> props.remove(props.post) }>Remove</MyButton>
      </div>
    </div>
  )
}

export default PostItem;