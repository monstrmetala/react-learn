
import React from "react";

const E404 = () => {
  

  return (
    <div className="page-404">
      <h2>404</h2>
      <h3>Page not found!</h3>
    </div>
  )
}

export default E404;