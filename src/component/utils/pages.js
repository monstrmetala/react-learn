export const getPageCount = (totalPosts, limit) => {
  return Math.ceil(totalPosts / limit);
};

export const getPagesArray = (totalPages) => {

  let pagi = [];
  for (let i = 0; i < totalPages; i++) {
    pagi.push(i +1);
  }
  return pagi;
}
