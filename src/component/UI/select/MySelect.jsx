import React from 'react';

const MySelect = ({options, defaultValue, value, onChange}) => {

  return (
    <select
      value={value}
      onChange={ e => onChange(e.target.value) }
    >
      <option>{defaultValue}</option>
      {options.map( o => 
        <option key={o.value} value={o.value}>{o.name}</option>
      )}
    </select>
  )
}

export default MySelect;