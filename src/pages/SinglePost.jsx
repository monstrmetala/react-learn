
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import PostService from "../API/PostService";
import Loader from "../component/UI/Loader/Loader";
import { useFetching } from "../hooks/useFetching";

const SinglePost = () => {
  const params = useParams();
  const [post, setPost] = useState({});
  const [comments, setComments] = useState([]);

  const [fetchById, isLoading, error] = useFetching( async (id) => {
    const res = await PostService.getById(id);
    setPost(res.data);
  })

  const [fetchComments, isComLoading, comError] = useFetching( async (id) => {
    const res = await PostService.getCommentByPostId(id);
    setComments(res.data);
  })


  useEffect( () => {
    fetchById(params.id)
    fetchComments(params.id)
  }, []);

  return (
    <div>
      {isLoading
        ? <Loader/>
        : <section>
            <h1>{post.title} ({post.id})</h1>
            <article>{post.body}</article>
            <hr/>
            Comments
            <hr/>
            <div>
              {isComLoading
                ? <Loader/>
                : <div className="post__comments">
                  {
                    comments.map( c => 
                      <div className="post__comment">
                        <ul>
                          <li><b>User name:</b> <span>{c.name}</span></li>
                          <li><b>User email:</b> <span>{c.email}</span></li>
                          <li><b>User comment:</b> <span>{c.body}</span></li>
                        </ul>
                        <hr/>
                      </div>
                    )
                  }
                </div>
              }
            </div>
          </section>
      }
    </div>
  )
}

export default SinglePost;