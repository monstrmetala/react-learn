import { CSSTransition, TransitionGroup } from "react-transition-group";
import PostItem from "./PostItem";


const PostList = ({posts, title, remove}) => {

  if (!posts.length) {
    return (
      <h2>Posts list is Empty</h2>
    )
  } 

  return (
    <div className="post-list">
      <h1 style={{textAlign: 'center'}}>{title}</h1>

      <TransitionGroup>
        {
          posts.map((p, index) => 
            <CSSTransition 
                key={p.id}
                timeout={500}
                className="post"
                >
              <PostItem remove={remove} number={index + 1} post={p} /> 
            </CSSTransition>
          )
        }
      </TransitionGroup>
    </div>
  )
}

export default PostList;