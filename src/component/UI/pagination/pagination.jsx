import React from 'react';
import { getPagesArray } from '../../utils/pages';


const Pagination = ({totalPages, page, changePage }) => {

  let pagiList = getPagesArray( totalPages ) ;

  return (
    <div className='pagination'>
      {
        pagiList.map(p => 
            <button 
              key={p}
              className={page === p ? 'current' : '' }
              onClick={() => changePage(p)}
            >{p}</button>
          )

      }
      </div>
  )
}

export default Pagination;