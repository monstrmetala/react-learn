import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Header from './component/Header';
import About from './pages/About';
import E404 from './pages/E404';
import Posts from './pages/Posts';
import SinglePost from './pages/SinglePost';

import './styles/App.css';

function App() {

  return (
    <div className="App">
      

      <BrowserRouter>
        <Header/>

        <Routes>
          <Route path='/' element={<h1>Homepage</h1>} />
          <Route path='/about' element={<About />} />
          <Route path='/posts' >
            <Route index element={<Posts />} />
            <Route path=':id' element={<SinglePost />} />
          </Route>
          <Route path='*' element={<E404/>} />
        </Routes>

      </BrowserRouter>

    </div>
  );
}

export default App;
